package com.chutkies.jokes.bot;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import static android.content.Context.MODE_PRIVATE;

public class Accessories  {

    //Write to a local file
    public static void writeFile(final String fileString, String fileName, Context context) {

        try {
            FileOutputStream fileOutputStream = context.openFileOutput(fileName, MODE_PRIVATE);
            fileOutputStream.write(fileString.getBytes());
            fileOutputStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //Read from a local file
    public static StringBuilder readFile(final String fileName, final Context context){
        final StringBuilder stringBuilder = new StringBuilder();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    FileInputStream fileInputStream = context.openFileInput(fileName.trim());

                    InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);

                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                    String lines;
                    while ((lines = bufferedReader.readLine()) != null) {
                        stringBuilder.append(lines);
                    }

                } catch (FileNotFoundException e) {
                    Log.d("file", "File not found............................. ");
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }).start();
        return stringBuilder;
    }


    protected static class DownloadFileAsyncTask extends AsyncTask<String,Integer,String>{

        private WeakReference<MainActivity> activityWeakReference;
        private String fileName = "";

        DownloadFileAsyncTask(MainActivity activity){
            activityWeakReference = new WeakReference<MainActivity>(activity);
        }
        @Override
        protected String doInBackground(String... strings) {

            MainActivity activity = activityWeakReference.get();
            if(activity == null || activity.isFinishing()){
                return null;
            }
            StringBuilder stringBuilder = new StringBuilder();
            try{
                URL url = new URL(strings[0]);
                fileName = strings[1];
                HttpsURLConnection uc = (HttpsURLConnection) url.openConnection();

                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream()));
                String line;

                while ((line = br.readLine()) != null) {
                    stringBuilder.append(line);
                }
                Log.i("DownLoad","Download Success");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return stringBuilder.toString();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            MainActivity activity = activityWeakReference.get();
            if(activity == null || activity.isFinishing()){
                return ;
            }
            writeFile(s,fileName,activity);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {

            super.onProgressUpdate(values);
        }
    }


}
