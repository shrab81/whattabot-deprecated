package com.chutkies.jokes.bot;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.chutkies.jokes.bot.Accessories.*;

public class MainActivity extends AppCompatActivity {

    private StringBuilder stringBuilder;
    private ListView listView;
    private ArrayList<String> myList;
    private boolean nextJoke = false;
    private StringBuilder stringBuilderQ ;
    private StringBuilder stringBuilderP ;
    private int count = 0;
    private TextView name;

    private StringBuilder stringBuilderStartup ;
    private StringBuilder stringBuilderMainLoop ;
    private StringBuilder stringBuilderJokes ;
    private StringBuilder stringBuilderKnockKnock ;
    private StringBuilder stringBuilderFeedBack ;
    private StringBuilder stringBuilderShutDown ;

    private List<Response> responses = null ;
    private final HappyBee happyBee = new HappyBee();
    private List<String> userInputList = new ArrayList<String>();
    private boolean isSleep = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Mobile Add
        initializeAdView();

        //Welcome message
        listView = findViewById(R.id.listView);
        myList = new ArrayList<String>();

        final HappyBee happyBee = new HappyBee();
        happyBee.downloadHappyBee(MainActivity.this);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    MainActivity.this.stringBuilderStartup = readFile("startup.json", MainActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    MainActivity.this.stringBuilderMainLoop = readFile("mainLoop.json", MainActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    MainActivity.this.stringBuilderJokes = readFile("jokes.json", MainActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    MainActivity.this.stringBuilderKnockKnock = readFile("knockKnock.json", MainActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    MainActivity.this.stringBuilderFeedBack = readFile("feedBack.json", MainActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    MainActivity.this.stringBuilderShutDown = readFile("shutDown.json", MainActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }// end of on create

    public void startButton(View view){

        Button startButton = findViewById(R.id.startButton);
        startButton.setVisibility(View.GONE);
        if(stringBuilderStartup != null && !stringBuilderStartup.toString().equals("")){
            happyBee.initializeHappyBee(stringBuilderStartup, "StartUp");
            happyBee.initializeHappyBee(stringBuilderMainLoop,"MainLoop");
            happyBee.initializeHappyBee(stringBuilderKnockKnock,"KnockKnock");
            happyBee.initializeHappyBee(stringBuilderFeedBack,"FeedBack");
            happyBee.initializeHappyBee(stringBuilderShutDown,"ShutDown");
            happyBee.initializeHappyBee(stringBuilderJokes,"Jokes");
            responses = happyBee.getResponses(userInputList,"");
            if(responses!=null && !responses.isEmpty()) {
                displayResponse(responses);
            }
        }else{
            startButton.setVisibility(View.VISIBLE);
            startButton.setText("Click again to Start...");
        }
    }


    private void timerTyping(int sec){
        new CountDownTimer(sec*1000, 1000) {
            String typing = "Typing.. ";
            ArrayAdapter<String> arrayAdapter = null;
            public void onTick(long millisUntilFinished) {
                typing = typing + "..";
                myList.add(myList.size(),typing);
                arrayAdapter = new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_list_item_1,myList);
                listView.setAdapter(arrayAdapter);
                arrayAdapter.notifyDataSetChanged();
                myList.remove(myList.size()-1);
            }

            public void onFinish() {
                //  mTextField.setText("done!");
            }
        }.start();
    }


    //Initial messages
    private void displayResponse(List<Response> responses){
        for(Response response : responses){
            String actionName = response.getActionName();
            ArrayAdapter<String> arrayAdapter = null;
            List<String> messages = response.getMessages();
            List<String> choices = response.getChoices();
            if(messages!= null &&!messages.isEmpty()){
                for(String message : messages){
                    myList.add(message);
                    arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,myList);
                    listView.setAdapter(arrayAdapter);
                    arrayAdapter.notifyDataSetChanged();
                    timerTyping(5);
                }
            }
            if(choices!= null&& !choices.isEmpty()){
               final LinearLayout linearLayout = findViewById(R.id.buttonLayout);
               final List<Button> buttonList = new ArrayList<>();
                for(int i = 0; i< choices.size();i++){
                   final Button button = new Button(this);
                   button.setLayoutParams(new LinearLayout.LayoutParams(500,150));
                   button.setId(i);
                   button.setText(choices.get(i));
                    buttonList.add(button);
                   button.setOnClickListener(new View.OnClickListener() {
                       @Override
                       public void onClick(View view) {
                           List<String> inputList = new ArrayList<>();
                           inputList.add(button.getText().toString());
                           List<Response> responses = happyBee.getResponses(inputList,"Choice");
                           if(responses!=null && !responses.isEmpty()) {
                               displayResponse(responses);
                           }
                           for(Button buttonTemp : buttonList) {
                               linearLayout.removeView(buttonTemp);
                           }
                           buttonList.clear();
                       }
                   });
                   linearLayout.addView(button);
                }
            }
            if(actionName!=null&&actionName.equals("SaveAsUserName")){
                String textHint = response.getFreeTextHint();
                TextView name = findViewById(R.id.editText);
                name.setVisibility(View.VISIBLE);
                name.setHint(textHint);
            }
        }



    }

    // Get User Inputs
    public void getEntry(View view){
        ArrayAdapter<String> arrayAdapter = null;
        name = findViewById(R.id.editText);
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(name.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

        if(stringBuilderMainLoop != null && !stringBuilderMainLoop.toString().equals("")){
            userInputList.add(name.getText().toString());
            responses = happyBee.getResponses(userInputList,"FreeFormText");
            if(responses!=null && !responses.isEmpty()) {
                displayResponse(responses);
            }
        }
        name.setVisibility(View.GONE);
    }



//Read next joke
    private void getNextJoke(){

        String jokesStringJSON;
        try {
            Random random = new Random();
            stringBuilderQ = new StringBuilder();
            stringBuilderP = new StringBuilder();
            jokesStringJSON = stringBuilder.toString();
            JSONObject root = new JSONObject(jokesStringJSON);

            JSONArray jokes = root.getJSONArray("jokes");
            int max = jokes.length();
            int index = random.nextInt(max);

            JSONObject joke = jokes.getJSONObject(index);
            stringBuilderQ.append(joke.getString("Question"));
            stringBuilderP.append(joke.getString("Punchline"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        nextJoke = false;
    }
    //process startup.json
    public void exit(View view){
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    private void initializeAdView(){
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        AdView mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });
    }



}