package com.chutkies.jokes.bot;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class HappyBee {
    private String urlStartup;
    private String urlMainLoop;
    private String urlStringJokes;
    private String urlKnockKnockJoke;
    private String urlFeedback;
    private String urlShutDown;

    private Response responseObj;
    public List<Choice> currentChoices;
    public List<Conversation> parentConversations;
    public Conversation currentConversation;
    public int parentConversationIndex = 0;
    public List<Conversation> parentStartUpConversations;
    public List<Conversation> parentKnockKnockConversations;
    public List<Conversation> parentMainLoopConversations;
    public List<Conversation> parentFeedBackConversations;
    public List<Conversation> parentShutDownConversations;
    public List<Joke> jokeList;
    private Joke currentJoke;
    private List<String> responseMessages = new ArrayList<String>();

    public HappyBee(){
        this.urlStartup = "https://jokelist.blob.core.windows.net/knockknockjokes/1.Startup.json?sp=rl&st=2020-08-17T22:11:25Z&se=2021-08-18T22:11:00Z&sv=2019-12-12&sr=b&sig=zwQLPNnVPnVjs3RRe9gdXwqhXzebx551Vf4b9XPLqIY%3D";
        this.urlMainLoop = "https://jokelist.blob.core.windows.net/knockknockjokes/2.MainLoop.json?sp=r&st=2020-09-04T02:07:19Z&se=2021-09-04T10:07:19Z&spr=https&sv=2019-12-12&sr=b&sig=oLLlon9F9NEDgTxCHMeWv9TzeNV3r15Wz2Fr56hriXA%3D";
        this.urlStringJokes = "https://jokelist.blob.core.windows.net/knockknockjokes/jokes.json?sp=r&st=2020-08-18T22:48:52Z&se=2022-01-02T07:48:52Z&spr=https&sv=2019-12-12&sr=b&sig=4%2BpsG057S1x43mn8xSODPt4z0cs4u3PlBQKUSfXVBEw%3D";
        this.urlKnockKnockJoke ="https://jokelist.blob.core.windows.net/knockknockjokes/3.KnockKnockJoke.json?sp=r&st=2020-09-04T02:15:49Z&se=2021-09-04T10:15:49Z&spr=https&sv=2019-12-12&sr=b&sig=pIIdx1ojEIjh1eQ0XJAJWPSGU9zjG8gnjyJw3Zg4qpA%3D";
        this.urlFeedback = "https://jokelist.blob.core.windows.net/knockknockjokes/4.Feedback.json?sp=r&st=2020-09-04T02:17:05Z&se=2021-09-04T10:17:05Z&spr=https&sv=2019-12-12&sr=b&sig=X2VMWiCMkPCHeDsjQWhGha5lb4570ImPReZiw1w%2FUuo%3D";
        this.urlShutDown = "https://jokelist.blob.core.windows.net/knockknockjokes/5.ShutDown.json?sp=r&st=2020-09-04T02:17:51Z&se=2021-09-04T10:17:51Z&spr=https&sv=2019-12-12&sr=b&sig=%2FxH2gJ84adJy9WpJQmknQwMECNanrryG7v3ng3Tzh90%3D";
    }

    public void downloadHappyBee(final Context context)
    {
        //pull the jsons down from public server (blob)
        //download startup file

        Accessories.DownloadFileAsyncTask downloadFileStartupAsyncTask = new Accessories.DownloadFileAsyncTask((MainActivity) context);
        downloadFileStartupAsyncTask.execute(urlStartup,"startup.json");

        downloadFileStartupAsyncTask = new Accessories.DownloadFileAsyncTask((MainActivity) context);
        downloadFileStartupAsyncTask.execute(urlMainLoop,"mainLoop.json");

        downloadFileStartupAsyncTask = new Accessories.DownloadFileAsyncTask((MainActivity) context);
        downloadFileStartupAsyncTask.execute(urlStringJokes,"jokes.json");

        downloadFileStartupAsyncTask = new Accessories.DownloadFileAsyncTask((MainActivity) context);
        downloadFileStartupAsyncTask.execute(urlKnockKnockJoke,"knockKnock.json");

        downloadFileStartupAsyncTask = new Accessories.DownloadFileAsyncTask((MainActivity) context);
        downloadFileStartupAsyncTask.execute(urlFeedback,"feedBack.json");

        downloadFileStartupAsyncTask = new Accessories.DownloadFileAsyncTask((MainActivity) context);
        downloadFileStartupAsyncTask.execute(urlShutDown,"shutDown.json");
    }

    //Process startup json
    public void initializeHappyBee( StringBuilder stringBuilder, String stringBuilderType){

        String startupString = stringBuilder.toString();
        try {
            if(stringBuilderType.equals("Jokes")){
                jokeList = getJokeObjList(stringBuilder);
                return;
            }
            JSONObject root = new JSONObject(startupString);
            JSONArray conversationsJsonArray = root.getJSONArray("Conversation");
            List<Conversation> conversationObjList = new ArrayList<Conversation>();

            for(int i= 0; i<conversationsJsonArray.length();i++){
                JSONObject conversationJson = conversationsJsonArray.getJSONObject(i);
                Conversation conversation = new Conversation();
                conversationObjList.add(setAndGetConversation(conversation,conversationJson));
            }
            if(!(conversationObjList == null) && !conversationObjList.isEmpty()) {
                if(stringBuilderType.equals("MainLoop")) {
                    parentMainLoopConversations = conversationObjList;
                }else if(stringBuilderType.equals("KnockKnock")){
                    parentKnockKnockConversations = conversationObjList;
                }else if(stringBuilderType.equals("StartUp")){
                    parentStartUpConversations =  conversationObjList;
                }else if(stringBuilderType.equals("FeedBack")){
                    parentFeedBackConversations = conversationObjList;
                }else if(stringBuilderType.equals("ShutDown")){
                    parentShutDownConversations = conversationObjList;
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private List<Joke> getJokeObjList(StringBuilder jokesStringBuilder) throws JSONException {

        JSONObject root = new JSONObject(jokesStringBuilder.toString());
        JSONArray jokesJsonArray = root.getJSONArray("jokes");
        jokeList = new ArrayList<Joke>();

        for(int i= 0; i<jokesJsonArray.length();i++){
            JSONObject jokeJson = jokesJsonArray.getJSONObject(i);
            Joke joke = new Joke();
            joke.setId(jokeJson.getString("ID"));
            joke.setPunchLine(jokeJson.getString("Punchline"));
            joke.setQuestion(jokeJson.getString("Question"));
            jokeList.add(joke);
        }
        return jokeList;
    }

    private Conversation setAndGetConversation(Conversation conversation, JSONObject conversationJson){

        StringBuilder typeSB = new StringBuilder();
        try {
                typeSB.append(conversationJson.getString("Type"));
                Conversation.Type selectedType;
                switch (typeSB.toString()) {
                    case "Check":
                        selectedType = Conversation.Type.Check;
                        conversation.setSelectedType(selectedType);

                        StringBuilder conditionSB = new StringBuilder();
                        StringBuilder actionsSB = new StringBuilder();
                        conditionSB.append(conversationJson.getString("Condition"));
                        actionsSB.append(conversationJson.getString("Action"));

                        conversation.setCondition(conditionSB.toString());
                        JSONArray actions = new JSONArray(actionsSB.toString());
                        conversation.setActions(setAndGetActions(actions));
                        break;
                    case "Action":
                        selectedType = Conversation.Type.Action;
                        conversation.setSelectedType(selectedType);
                        conversation.setActionName(conversationJson.getString("ActionName"));
                        conversation.setSaveResultTo(conversationJson.getString("SaveResultTo"));
                        break;
                    case "Goto":
                        selectedType = Conversation.Type.Goto;
                        conversation.setSelectedType(selectedType);

                        conversation.setGotoTarget(conversationJson.getString("GotoTarget"));
                        break;
                    case "Message":
                        selectedType = Conversation.Type.Message;
                        conversation.setSelectedType(selectedType);
                        JSONArray messageJsonArray = conversationJson.getJSONArray("Message");
                        conversation.setMessages(setAndGetMessages(messageJsonArray));
                        JSONObject expectedResponseJson = conversationJson.getJSONObject("ExpectedResponse");
                        conversation.setExpectedResponse(setAndGetExpectedResponse(expectedResponseJson));
                        break;
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        return conversation;
    }

    private ExpectedResponse setAndGetExpectedResponse( JSONObject expectedResponseJson) throws JSONException {
        ExpectedResponse expectedResponse = new ExpectedResponse();

        expectedResponse.setInputType(expectedResponseJson.getString("InputType"));

        if(expectedResponseJson.getString("InputType").equals("FreeFormText")) {
            expectedResponse.setInputAction(expectedResponseJson.getString("InputAction"));
            expectedResponse.setInputHint(expectedResponseJson.getString("InputHint"));
            JSONArray conversationsJsonArray = expectedResponseJson.getJSONArray("Conversation");
            if (conversationsJsonArray != null && !conversationsJsonArray.equals("")) {
                List<Conversation> conversationNestedObjList = new ArrayList<Conversation>();
                for (int j = 0; j < conversationsJsonArray.length(); j++) {
                    JSONObject conversationNestedJson = conversationsJsonArray.getJSONObject(j);
                    Conversation conversationNestedObj = new Conversation();

                    conversationNestedObjList.add(setAndGetConversation(conversationNestedObj, conversationNestedJson));
                }
                expectedResponse.setConversations(conversationNestedObjList);
            }
        }
        if(expectedResponseJson.getString("InputType").equals("choices")) {
            JSONArray choicesJsonArray = expectedResponseJson.getJSONArray("Choices");
            if (choicesJsonArray != null && !choicesJsonArray.equals("")) {
                List<Choice> choiceObjList = new ArrayList<Choice>();
                for (int j = 0; j < choicesJsonArray.length(); j++) {
                    JSONObject choiceJson = choicesJsonArray.getJSONObject(j);
                    Choice choiceObj = new Choice();

                    choiceObjList.add(setAndGetChoice(choiceObj, choiceJson));
                }
                expectedResponse.setChoices(choiceObjList);
            }
        }
        return expectedResponse;
    }

    private Choice setAndGetChoice(Choice choice, JSONObject choiceJson)throws JSONException{
        JSONArray optionsJsonArray = choiceJson.getJSONArray("Option");

        List<String> optionsStringList = new ArrayList<>();
        for(int i= 0; i<optionsJsonArray.length();i++){
            String optionJson = optionsJsonArray.getString(i);
            optionsStringList.add(optionJson);
        }
        choice.setOptions(optionsStringList);

        JSONArray conversationsJsonArray = choiceJson.getJSONArray("Conversation");
        List<Conversation> conversationNestedObjList = new ArrayList<Conversation>();
        for(int j= 0; j<conversationsJsonArray.length();j++) {
            JSONObject conversationNestedJson = conversationsJsonArray.getJSONObject(j);
            Conversation conversationNestedObj = new Conversation();

            conversationNestedObjList.add(setAndGetConversation(conversationNestedObj, conversationNestedJson));
        }
        choice.setConversationList(conversationNestedObjList);

        return choice;
    }

    private List<String> setAndGetMessages( JSONArray messages) throws JSONException {
        List<String> messagesObjList = new ArrayList();

        for(int j= 0; j<messages.length();j++) {
            messagesObjList.add((String)messages.get(j));
        }
        return messagesObjList;
    }

    private List<Action> setAndGetActions(JSONArray actions) throws JSONException {

        List<Action> actionsObjList = new ArrayList<>();
        for(int i= 0; i<actions.length();i++) {
            JSONObject actionJson = actions.getJSONObject(i);

            StringBuilder resultSB = new StringBuilder();
            resultSB.append(actionJson.getString("Result"));

            Action actionObj = new Action();
            if(resultSB.toString().equals("true"))
                actionObj.setResult(true);
            else
                actionObj.setResult(false);

            JSONArray conversationsJsonArray = actionJson.getJSONArray("Conversation");
            List<Conversation> conversationNestedObjList = new ArrayList<Conversation>();
            for(int j= 0; j<conversationsJsonArray.length();j++) {
                JSONObject conversationNestedJson = conversationsJsonArray.getJSONObject(j);
                Conversation conversationNestedObj = new Conversation();

                conversationNestedObjList.add(setAndGetConversation(conversationNestedObj, conversationNestedJson));
            }
            actionObj.setConversations(conversationNestedObjList);
            actionsObjList.add(actionObj);
        }

        return actionsObjList;
    }

///////////////////////////////////////////////////////// Process//////////////////////////////////////////
    public static boolean isUserNamePresent(){
        return false;
    }

    public List<Response> getResponses(List<String> userInput, String userInputType) {
        if (userInputType.equals("")){
            for(Conversation conversation: parentStartUpConversations) {
                processCurrentConversation(userInput, conversation);
            }
        } else if(userInputType.equals("Choice")) {
            if(currentChoices==null){
                currentChoices = currentConversation.getExpectedResponse().getChoices();
            }
            processCurrentChoice(userInput, currentChoices);
        } else if(userInputType.equals("FreeFormText")) {
            for(Conversation conversation: parentMainLoopConversations) {
                processCurrentConversation(userInput, conversation);
            }
        }
        List<Response> responseList = new ArrayList<>();
        Response response = this.responseObj;
        if(response!=null)
            responseList.add(response);
        return responseList;
    }

    private void processCurrentConversation(java.util.List<String> userInput, Conversation conversationObj){
        currentConversation = conversationObj;
        ExpectedResponse expectedResponse;
        Conversation.Type type = currentConversation.getSelectedType();
        switch (type) {
            case Goto:
                chooseGoto(currentConversation);

                break;
            case Check:
                boolean result = true;
                if(currentConversation.getCondition().equals("UserNamePresent")){
                    result = isUserNamePresent();
                }
                for(Action currentAction: currentConversation.getActions()){
                    if(currentAction.isResult()== result){
                        List<Conversation> conversations = currentAction.getConversations();
                        for(Conversation conversation: conversations) {
                            processCurrentConversation(userInput, conversation);
                        }
                    }
                }

                break;
            case Action:
                chooseAction(currentConversation);

                break;
            case Message:
                this.responseObj = new Response();
                String responseMessage;

                for(String message:currentConversation.getMessages()) {
                    if (message.contains("$UserName")) {
                        responseMessage = message.replace("$UserName", userInput.get(0));
                    } else if (message.contains("$CurrentJoke.Line1")) {
                        responseMessage =  message.replace("$CurrentJoke.Line1",currentJoke.getQuestion());
                    }else if (message.contains("$CurrentJoke.Line2")) {
                        responseMessage = currentJoke.getPunchLine();
                    }else{
                        responseMessage = message;
                    }
                    responseMessages.add(responseMessage);
                }
                this.responseObj.setMessages(responseMessages);
                expectedResponse = currentConversation.getExpectedResponse();
                if(!(expectedResponse==null)) {
                    processExpectedResponse(expectedResponse);
                    responseMessages = new ArrayList<>();
                }else{
                    processCurrentConversation(userInput,parentConversations.get(parentConversationIndex));
                }
                break;
            default:

                break;
        }


    }

    private Joke pickAJoke(){
        Random random = new Random();
        int max = jokeList.size()-1;
        int index = random.nextInt(max);
        Joke joke = jokeList.get(index);
        return joke;
    }

    private void processCurrentChoice(List<String> userInput, List<Choice> choices){

        for(Choice choice:choices){
            List<String> optionList = choice.getOptions();

            Random random = new Random();
            int max = optionList.size() - 1;
            int index = 0;
            if(max>0)
                index = random.nextInt(max);

            if(choice.getOptions().get(index).equals(userInput.get(0))){
                List<Conversation> currentNestedConversationList = choice.getConversationList();
                for(Conversation conversation: currentNestedConversationList) {
                    processCurrentConversation(userInput, conversation);
                }
                ExpectedResponse expectedResponse = currentConversation.getExpectedResponse();
                if(!(expectedResponse==null)) {
                    processExpectedResponse(expectedResponse);
                }
            }
        }
    }

    private void processExpectedResponse(ExpectedResponse expectedResponse){
        if(expectedResponse.getInputType().equals("FreeFormText")) {
            this.responseObj.setActionName(expectedResponse.getInputAction());
            this.responseObj.setFreeTextHint(expectedResponse.getInputHint());
            this.responseObj.setResponseExpected(true);
            this.responseObj.setSelectedResponseType(Response.ResponseType.FreeText);
        }
        if(expectedResponse.getInputType().equals("choices")){
            this.responseObj.setResponseExpected(true);
            currentChoices = new ArrayList<Choice>();

            List<String> choicesStrings = new ArrayList<>();
            for(Choice choice:expectedResponse.getChoices()){
                Choice choiceTemp = new Choice();
                choiceTemp.setExpectedResponse(choice.getExpectedResponse());
                choiceTemp.setConversationList(choice.getConversationList());
                choiceTemp.setOptions(choice.getOptions());
                if (choiceTemp.getOptions().get(0).contains("$CurrentJoke.Line1")) {
                    List<String> optionsTemp = new ArrayList<>();
                    optionsTemp.add(choice.getOptions().get(0).replace("$CurrentJoke.Line1",currentJoke.getQuestion()));
                    choiceTemp.setOptions(optionsTemp);
                }

                currentChoices.add(choiceTemp);
                choicesStrings.add(choiceTemp.getOptions().get(0));
            }

            this.responseObj.setChoices(choicesStrings);
        }
    }

    private void saveAsUserName(String userName){

    }

    private void gotoMainLoop(){
        for(Conversation conversation: parentMainLoopConversations) {
            processCurrentConversation(null, conversation);
        }
    }

    private void gotoKnockKnockJoke(){
        parentConversations = parentKnockKnockConversations;
        parentConversationIndex = 0;
        for(Conversation conversation:parentKnockKnockConversations) {
            processCurrentConversation(null, conversation);
            parentConversationIndex ++;
            if(conversation.getExpectedResponse()!=null) {
                break;
            }
        }
    }

    private void gotoShutDown(){
        parentConversations = parentShutDownConversations;
        parentConversationIndex = 0;
        for(Conversation conversation: parentShutDownConversations) {
            processCurrentConversation(null, conversation);
            parentConversationIndex++;
            if(conversation.getExpectedResponse()!=null) {
                break;
            }
        }
    }

    private void gotoFeedBack(){
        parentConversations = parentFeedBackConversations;
        parentConversationIndex = 0;
        for(Conversation conversation: parentFeedBackConversations) {
            processCurrentConversation(null, conversation);
            parentConversationIndex++;
            if(conversation.getExpectedResponse()!=null) {
                break;
            }
        }
    }

    private void chooseGoto(Conversation conversation){
        if(currentConversation.getGotoTarget().equals("2.MainLoop")) {
            gotoMainLoop();
        }else if(currentConversation.getGotoTarget().equals("3.KnockKnockJoke")) {
            gotoKnockKnockJoke();
        }else if(currentConversation.getGotoTarget().equals("5.ShutDown")) {
            gotoShutDown();
        }else if(currentConversation.getGotoTarget().equals("4.Feedback")) {
            gotoFeedBack();
        }
    }
    private void chooseAction(Conversation conversation) {
        ExpectedResponse expectedResponse = conversation.getExpectedResponse();
        switch (currentConversation.getActionName()) {
            case "PickAKnockKnockJoke":
                currentJoke = pickAJoke();
                break;
            case "SaveToFavorite":
                saveToFavorite();
                expectedResponse = currentConversation.getExpectedResponse();
                if(!(expectedResponse==null)) {
                    processExpectedResponse(expectedResponse);
                }else{
                    processCurrentConversation(null,parentConversations.get(parentConversationIndex));
                }
                break;
            case "LikeIt":
                likeIt();
                if(!(expectedResponse==null)) {
                    processExpectedResponse(expectedResponse);
                }else{
                    processCurrentConversation(null,parentConversations.get(parentConversationIndex));
                }
                break;
            case "NotFunny":
                notFunny();
                if(!(expectedResponse==null)) {
                    processExpectedResponse(expectedResponse);
                }else{
                    processCurrentConversation(null,parentConversations.get(parentConversationIndex));
                }
                break;
            default:
                break;

        }
    }
    private void notFunny(){

    }
    private void likeIt(){

    }
    private void saveToFavorite() {
    }
    private void shutDownAction(){
        android.os.Process.killProcess(android.os.Process.myPid());
    }

}
