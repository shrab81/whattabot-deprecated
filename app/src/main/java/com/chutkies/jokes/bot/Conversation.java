package com.chutkies.jokes.bot;

import java.util.List;

public class Conversation {
    private Type selectedType;
    private String condition;
    private List<Action> actions;
    private String gotoTarget;
    private List<String> messages;
    private ExpectedResponse expectedResponse;
    private String actionName;
    private String saveResultTo;

    public Conversation() {
    }

    public Type getSelectedType() {
        return selectedType;
    }

    public void setSelectedType(Type selectedType) {
        this.selectedType = selectedType;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    public String getGotoTarget() {
        return gotoTarget;
    }

    public void setGotoTarget(String gotoTarget) {
        this.gotoTarget = gotoTarget;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public ExpectedResponse getExpectedResponse() {
        return expectedResponse;
    }

    public void setExpectedResponse(ExpectedResponse expectedResponse) {
        this.expectedResponse = expectedResponse;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getSaveResultTo() {
        return saveResultTo;
    }

    public void setSaveResultTo(String saveResultTo) {
        this.saveResultTo = saveResultTo;
    }

    public enum Type {
        Check,
        Message,
        Goto,
        Action;


    }
}
