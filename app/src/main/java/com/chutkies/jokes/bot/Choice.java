package com.chutkies.jokes.bot;

import java.util.List;

public class Choice {
    private List<String> options;
    private List<Conversation> conversationList;
    private ExpectedResponse expectedResponse;

    public Choice() {
    }

    public List<String> getOptions() {
        return options;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }

    public List<Conversation> getConversationList() {
        return conversationList;
    }

    public void setConversationList(List<Conversation> conversationList) {
        this.conversationList = conversationList;
    }

    public ExpectedResponse getExpectedResponse() {
        return expectedResponse;
    }

    public void setExpectedResponse(ExpectedResponse expectedResponse) {
        this.expectedResponse = expectedResponse;
    }
}
