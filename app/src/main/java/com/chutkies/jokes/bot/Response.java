package com.chutkies.jokes.bot;

import java.util.List;

public class Response {
    private List<String> messages;

    private List<String> choices;

    private String ActionName;

    private String freeTextHint;

    private boolean responseExpected;

    private ResponseType selectedResponseType;

    public Response() {

    }

    // "FreeText", "choices"
    public enum ResponseType{
        FreeText,
        Choices
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public List<String> getChoices() {
        return choices;
    }

    public void setChoices(List<String> choices) {
        this.choices = choices;
    }

    public String getActionName() {
        return ActionName;
    }

    public void setActionName(String actionName) {
        ActionName = actionName;
    }

    public String getFreeTextHint() {
        return freeTextHint;
    }

    public void setFreeTextHint(String freeTextHint) {
        this.freeTextHint = freeTextHint;
    }

    public boolean isResponseExpected() {
        return responseExpected;
    }

    public void setResponseExpected(boolean responseExpected) {
        this.responseExpected = responseExpected;
    }

    public ResponseType getSelectedResponseType() {
        return selectedResponseType;
    }

    public void setSelectedResponseType(ResponseType selectedResponseType) {
        this.selectedResponseType = selectedResponseType;
    }
}
