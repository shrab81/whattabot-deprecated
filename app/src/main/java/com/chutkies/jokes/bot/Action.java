package com.chutkies.jokes.bot;

import java.util.List;

public class Action {
    private boolean result;
    private List<Conversation> conversations;

    public Action(boolean result, List<Conversation> conversations) {
        this.result = result;
        this.conversations = conversations;
    }

    public Action() {
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public List<Conversation> getConversations() {
        return conversations;
    }

    public void setConversations(List<Conversation> conversations) {
        this.conversations = conversations;
    }
}
